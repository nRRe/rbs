#!/usr/bin/sparcv9/python2.7

import os, sys, time

## import my module :D
my_module_loc = os.path.join("C:\\","Users","nrre1","Documents","RBS","bin")
sys.path.append(my_module_loc)
import rbs_customs as rbs



## create lists we'll be using
final_list = []

print ("...")
print ("...")
main_path = os.path.join("C:\\","Users","nrre1","Documents","RBS","B017")
print ("Main Path: ", main_path)
raw_data_file = os.path.join(main_path,"raw_data","b017202008.csv")
print ("PM Data file: ", raw_data_file)
aisr_ccsd_file = os.path.join(main_path,"lib","aisr_list.csv")
print ("AISR CCSD List: ", aisr_ccsd_file)
output_file = os.path.join(main_path,"reports","rbs_aisr_b017_2020_08.csv")
print("AISR PM Data File: ", output_file)
print ("...")
print ("...")

add_line = 'CCSD,Date,BW,Port_L1_L3_UAS%,Port_L1_ES%,Port_L1_CV%,Path_L1_UAS%,Path_L1_ES%,Path_L1_CV%,L2_Frame_Loss%,L2_Error_Count,L2_Utilization%,L3_Packet_Loss%,L3_Utilization%,L3_Latency,CONUS,EUROPE,PAC,CENTRAL,AFRICOM,HL_CCSD,Service_Type\n'

# new way that takes 0.4 seconds .... incredible
with open(aisr_ccsd_file) as aisr_ccsd: # again aisr has 70 lines
    chosen_list = set([ line.strip() for line in aisr_ccsd ])

with open(raw_data_file,"r") as pm_data_file: #pm data has 404,000 lines
    for line in pm_data_file:
        ccsd = line.split(",")[0]
        if ccsd in chosen_list:
            final_list.append(line)

def ccsd_sort(final_list):
    return final_list.split(',')[0]

final_list.sort(key = ccsd_sort)
final_list.insert(0,add_line)

with open(output_file,"w") as file:
    os.chmod(output_file, 0o666)
    file.writelines(final_list)

print ("Completed")

## call up my module
rbs.scripttime()



'''
# WRITES WRITE TO FILE INSTEAD OF USING FINAL_LIST TAKES 4.5 SECONDS
with open(aisr_ccsd_file) as aisr_ccsd: # again aisr has 70 lines
    chosen_list = set([ line.strip() for line in aisr_ccsd ])
file = open(output_file,"w")
os.chmod(output_file,0o666)
file.writelines(add_line)
with open(raw_data_file,"r") as pm_data_file: #pm data has 404,000 lines
    for line in pm_data_file:
        ccsd = line.split(",")[0]
        if ccsd in chosen_list:
            file.writelines(line)
file.close()

#  OLD WAY THAT FOR SURE WORKS, TAKES 24.4-24.8 SECONDS
with open(aisr_ccsd_file) as aisr_ccsd: #aisr_ccsd_list has 70 lines of circuit IDs
    chosen_list = { line.strip() for line in aisr_ccsd }
print ("Pulling data on the following CCSDs")
print (chosen_list)
print ("...")
print ("...")
print ("Script is running, please hold.")
print ("Takes approx three minutes per day.")

for item in chosen_list:
    with open(raw_data_file,"r") as pm_data_file: # raw data is 404,353 lines of metrics
        for line in pm_data_file:
            splits = line.split(",")
            ccsd = splits[0]
            if ccsd == item:
                final_list.append(line)

with open(output_file,"w") as file:
    os.chmod(output_file, 0o666)
    file.writelines(final_list)

# OLD WAY, MUCH CONVOLUTED, ABOVE CODE MUCH CLEANER.  TAKES 25 SECONDS
for item in chosen_list:
    with open(raw_data_file,"r") as pm_data_file:
        for line in pm_data_file:
            splits = line.split(',')
            router = splits[0]
            element = splits[1]
            ccsd = splits[2]
            label = splits[3]
            bandwidth = splits[4]
            highlevel = splits[5]
            servicetype = splits[6]
            pmtype = splits[7]
            pmvalue = splits[8] .replace("\n","")
            if ccsd == item:
                write_line = "{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}\n" .format(raw_file_name,router,element,ccsd,label,bandwidth,highlevel,servicetype,pmtype,pmvalue)
                final_list.append(write_line)

with open(output_file, "w") as file:
	os.chmod(output_file, 0o666)
	file.writelines(final_list)
print ("Completed, please see file: ",output_file)
'''
