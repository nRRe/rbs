#!/usr/bin/sparcv9/python2.7

##########  AISR  ##########
########  DATA PULL  #######
#### Current Vers 1.2.1 ####
## Intent: pull AISR specified CCSDs  ##
## performance metrics data and compile  ##

########  HISTORY  ##########
## 2020/08/24 - Rev 0.1.1 - Alpha Release # #
## 2020/08/28 - Rev 1.0.0 - Working Release  ##
## 2020/08/30 - Rev 1.1.0 - Hard Coded CCSD list works  ##
## 2020/08/31 - Rev 1.2.0 - Flat File CCSD list works  ##
## 2020/09/03 - Rev 1.2.1 - Put into production ##

#########  FUTURE  #########
## Rev 1.3.0 - Selectable date range for data pull  ##

## Import the necessities
import os, glob, sys, datetime

## import my module :D
my_module_loc = os.path.join("/home","Common","Global_PM","ETL","bin")
sys.path.append(my_module_loc)
import rbs_customs as rbs


## create lists we'll be using
final_list = []

print ("...")
print ("...")
print ("...")

## set variables for dates
output_file_time = datetime.datetime.now()
raw_data_file_time = datetime.datetime.now() - datetime.timedelta(days=1)
## set parameters
path_main = os.path.join("/home","Common","Global_PM","ETL")
## print (path_main)
lib_fold = os.path.join(path_main,"lib")
## print (lib_fold)
raw_data_fold = os.path.join(path_main,"final","pm")
## print (raw_data_fold)
raw_file_name = raw_data_file_time.strftime('20%y%m%d') . replace(':','')
## print (raw_file_name)
raw_data_file = ("{0}/{1}" .format(raw_data_fold,raw_file_name))
print ("PM Data file: ", raw_data_file)
aisr_ccsd_file = ("{0}/aisr_list" .format(lib_fold))
print ("AISR CCSD List: ", aisr_ccsd_file)
## print(output_file_time)
output_file_name = output_file_time.strftime('20%y%m%d') . replace(':','')
output_file_fold = os.path.join(path_main,"Reports","aisr_daily")
## print(output_file_name)
output_file = ("{0}/{1}.csv" .format(output_file_fold,raw_file_name))
print("AISR PM Data File: ", output_file)
final_output_dir = os.path.join("/home","Common","AISR SUPPORT","PRI-1_PMDATA")
print ("...")
print ("...")
print ("...")
add_line = ["DATE,","ROUTER,","ELEMENT,","CCSD,","LABEL,","BANDWIDTH,","HIGHER-LEVEL,","SERVICE-TYPE,","PM-TYPE,","PM-VALUE\n"]

print ("Script is running, please hold.")
print ("Takes approx three minutes per day.")

## flat file for aisr list
with open(aisr_ccsd_file) as aisr_ccsd:
    chosen_list = set([ line.strip() for line in aisr_ccsd ])

## final/pm/yyyymmdd raw data file
with open(raw_data_file,"r") as pm_data_file:
    for line in pm_data_file:
        ccsd = line.split('\t')[2] # checks CCSD column
        if ccsd == chosen_list: # if it matches the aisr_ccsd_file list
            final_list.append(line) # adds it to the list to write to file

def sort_list(final_list):
    return final_list.split('\t')[3]

final_list.sort(key=sort_list) # sorts list, gotta figure out how to sort by ccsd=line.split[2]
final_list.insert(0,add_line) # inserts the header row at the top

with open(output_file, "w") as file: # write to global_pm folder
	os.chmod(output_file, 0o666)
	file.writelines(final_list)

with open(final_output_dir+raw_file_name+"-AISR_PM.csv", "w") as file2: # to aisr folder
	os.chmod(final_output_dir+raw_file_name+"-AISR_PM.csv", 0o666)
	file2.writelines(final_list)

print ("Completed, please see file: ",output_file)
rbs.scripttime()
