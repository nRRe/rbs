#!/usr/bin/python

import os, datetime, time
from pexpect import pxssh

main_path = os.path.join("/home","pi","Python","RBS")

## grab password
with open(os.path.join(main_path,"lib","cred"),"r") as pword:
    for line in pword:
        pword = line.strip()

raw_date = datetime.datetime.now() - datetime.timedelta(days=1)
raw_folder = raw_date.strftime("%d")

data_date = datetime.datetime.now()
data_file = data_date.strftime("%d")

print (raw_folder)
print (data_file)

try:
    s = pxssh.pxssh()
    hostname="192.168.1.2"
    username="root"
    password=pword
    s.login(hostname, username, password)
    s.sendline('cd /mnt/user/FTP')
    s.prompt()
    print(s.before)
    s.sendline('ls -lah')
    s.prompt()
    print(s.before)
    s.sendline('lcd /home/pi/Python/RBS/raw_data/{}' .format(raw_folder))  ## messes up at home environment
    s.prompt()
    print(s.before)
    s.sendline('pwd')
    s.prompt()
    print(s.before)
    s.sendline('lpwd')  ## messes up at home environment
    s.prompt()
    print(s.before)
    s.logout()
except pxssh.ExceptionPxssh as e:
    print ("pxssh failed on login.")
    print (e)