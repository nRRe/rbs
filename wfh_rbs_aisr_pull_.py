#!/usr/bin/python

##########  AISR  ##########
########  DATA PULL  #######
#### Current Vers 0.1.1 ####
## Intent: pull AISR specified CCSDs  ##
## performance metrics data and compile  ##
## Script will run nightly and store the ##
## data on OSS for archival purposes but ##
## unsure exaclty who much storage space ##

########  HISTORY ##########
## Rev 0.1.1 - Alpha  ##

#########  FUTURE  #########
## Rev 1.0.0 - Working Release  ##
## Rev 1.1.0 - Hard coded CCSDs  ##
## Rev 1.2.0 - CCSDs from flat file  ##
## Rev 1.3.0 - Selectable date range for data pull  ##

########## WARNING/NOTES ##########
## if you see the following after a line ##HOME_USE
## it is currently using my personal setup ##
## home environment and will switch to match ##
## works working environment when re-written ##

## Import the necessities
import os, glob, sys

## set raw data directory
path_main="/home/pi/Python/ETL/"        ##HOME_USE

## create working/data files
# working_data_file
# final_data

## Create the lists we'll work with
## AISR specific CCSD list
aisr_ccsd_list = set()
## Metrics data pulled
chosen_ones = []
## Compiled directory names
final_list = []

folder_list = os.listdir('/home/pi/Python/ETL/raw_data')       ##HOME_USE
for fold in folder_list:
    f_list = (fold+",")
    final_list.append (f_list)

print (final_list)


with open("/home/pi/Python/ETL/lib/node_list.csv", "w") as node_name_list:     ##HOME_USE
    node_name_list.writelines(final_list)

'''brain storming at home and came up with this nonsense

with open("{0}{1}/current/file.tsv" .format(path_main,final_list) as metrics_data:    ##HOME_USE
    for line in metrics_data:
        router = line.split('\t')[0]    ##HOME_USE
        circuit_id = line.split('\t')[1]     ##HOME_USE
        circuit_usage = line.split('\t')[2]     ##HOME_USE
        while circuit_id = aisr_ccsd_list:
            matching_data = "{0},{1}\n" .format(circuit_id,circuit_usage)     ##HOME_USE
            final_data.append(matching_data)
'''